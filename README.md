# Flask Monitoring Application with Auto-Scaling Alert

This Flask application provides real-time monitoring of CPU and memory utilisation using the psutil library. Upon accessing the homepage, users are presented with metrics indicating CPU and memory usage percentages. Additionally, if either the CPU utilisation exceeds 80% or memory usage exceeds 90%, an alert message is displayed, advising users to scale up their resources. The application is containerised using Docker for easy deployment and scalability. By leveraging Flask and psutil, this monitoring application offers insights into system performance and ensures proactive resource management.

## Features

- **Real-time Monitoring:** Provides CPU and memory usage metrics in real-time.
- **Auto-Scaling Alert:** Displays an alert message when CPU or memory utilisation exceeds predefined thresholds.
- **Containerised Deployment:** Dockerised for seamless deployment and scalability.
- **Easy Setup:** Simple setup with Docker for quick installation and usage.

## Usage

To run the application locally, follow these steps:

1. Clone the repository: `git clone https://gitlab.com/adharsh200595/mysystemmonitor`
2. Navigate to the project directory: `cd mysystemmonitor`
3. Build the Docker image: `docker build -t flask-monitoring-app .`
4. Run the Docker container: `docker run -p 5000:5000 flask-monitoring-app`

The application will be accessible at `http://localhost:5000`.

## Contributing

Contributions are welcome! Please fork the repository and create a pull request with your proposed changes.

## License

This project is licensed under the [MIT License](LICENSE).
